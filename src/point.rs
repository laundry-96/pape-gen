use log::debug;
use std::ops::{Add, AddAssign, Sub};

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

impl Point {
    /// Creates a new point, mostly used for implementing logging
    pub fn new(x: i32, y: i32) -> Point {
        debug!("Creating point ({:?}, {:?})", x, y);

        Point { x: x, y: y }
    }

    /// Rotates the current point around a center point, based on degrees.
    /// This method creates a new point and returns it.
    pub fn rotate(self, center: Point, radians: f64) -> Point {
        debug!(
            "Going to rotate {:?} around {:?}, {:?} radians",
            self, center, radians
        );

        let mut new_point = self;

        let sin = radians.sin();
        let cos = radians.cos();

        new_point.x -= center.x;
        new_point.y -= center.y;

        let x_new = ((new_point.x as f64 * cos) - (new_point.y as f64 * sin)).round();
        let y_new = ((new_point.x as f64 * sin) + (new_point.y as f64 * cos)).round();

        new_point.x = (x_new as i32 + center.x);
        new_point.y = (y_new as i32 + center.y);

        debug!("New rotated point is {:?}", new_point);

        new_point
    }

    /// Checks if the point (self) is INSIDE the other point.
    /// This also checks to make sure that we are within 0, 0
    pub fn in_bounds(&self, other_point: Point) -> bool {
        let other_inside_bounds = (other_point.x >= 0) && (other_point.y >= 0);
        let self_inside_bounds = (self.x >= 0) && (self.y >= 0);

        let is_inside = (other_point.x > self.x)
            && (other_point.y > self.y)
            && other_inside_bounds
            && self_inside_bounds;

        debug!("Is {:?} in {:?}: {:?}", self, other_point, is_inside);

        is_inside
    }
}

impl Add for Point {
    type Output = Point;

    fn add(self, other: Point) -> Point {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Point {
    type Output = Point;

    fn sub(self, other: Point) -> Point {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl AddAssign for Point {
    fn add_assign(&mut self, other: Point) {
        self.x += other.x;
        self.y += other.y;
    }
}

#[test]
fn rotate_test_1() {
    let mut point = Point { x: 10, y: 0 };

    let center_point = Point { x: 0, y: 0 };

    let radians: f64 = 3.14 / 2.0;
    point = point.rotate(center_point, radians);

    let final_point = Point { x: 0, y: 10 };

    assert_eq!(point, final_point);
}
#[test]
fn rotate_test_2() {
    let mut point = Point { x: 10, y: 5 };

    let center_point = Point { x: 5, y: 5 };

    let radians: f64 = 3.14 / 2.0;
    point = point.rotate(center_point, radians);

    let final_point = Point { x: 5, y: 10 };

    assert_eq!(point, final_point);
}

#[test]
fn rotate_test_3() {
    let mut point = Point { x: 3, y: 4 };

    let center_point = Point { x: 0, y: 0 };

    let radians = 3.14;
    point = point.rotate(center_point, radians);

    let final_point = Point { x: -3, y: -4 };

    println!("out: {:?}", point);
    assert_eq!(point, final_point);
}

#[test]
fn rotate_test_4() {
    let mut point = Point { x: 3, y: 4 };

    let center_point = Point { x: 2, y: 2 };

    let radians = 3.14;
    point = point.rotate(center_point, radians);

    let final_point = Point { x: 1, y: 0 };

    assert_eq!(point, final_point);
}

#[test]
fn rotate_test_5() {
    let mut point = Point { x: -7, y: 2 };

    let center_point = Point { x: 5, y: 9 };

    let radians = 4.2;
    point = point.rotate(center_point, radians);

    let final_point = Point { x: 5, y: 23 };

    assert_eq!(point, final_point);
}

#[test]
fn rotate_test_6() {
    let mut point = Point { x: -9, y: -6 };

    let center_point = Point { x: -20, y: -20 };

    let radians = 5.0;
    point = point.rotate(center_point, radians);

    let final_point = Point { x: -3, y: -27 };

    assert_eq!(point, final_point);
}

#[test]
fn bounds_test_1() {
    let big_point = Point { x: 50, y: 50 };

    let small_point = Point { x: 25, y: 25 };

    assert!(small_point.in_bounds(big_point));
}

#[test]
fn bounds_test_2() {
    let big_point = Point { x: 50, y: 50 };

    let small_point = Point { x: 49, y: 49 };

    assert!(small_point.in_bounds(big_point));
}

#[test]
fn bounds_test_3() {
    let big_point = Point { x: 50, y: 50 };

    let small_point = Point { x: 50, y: 25 };

    assert!(!small_point.in_bounds(big_point));
}

#[test]
fn bounds_test_4() {
    let big_point = Point { x: 50, y: 50 };

    let small_point = Point { x: 25, y: 50 };

    assert!(!small_point.in_bounds(big_point));
}

#[test]
fn bounds_test_5() {
    let big_point = Point::new(5, 5);
    let small_point = Point::new(-1, -1);

    assert!(!small_point.in_bounds(big_point));
}

#[test]
fn add_test_1() {
    let a = Point { x: 1, y: 1 };
    let b = Point { x: 1, y: 1 };

    let c = a + b;

    assert_eq!(c, Point { x: 2, y: 2 });
}

#[test]
fn add_test_2() {
    let a = Point { x: 1, y: 1 };
    let b = Point { x: 2, y: 2 };

    let c = a + b;

    assert_eq!(c, Point { x: 3, y: 3 });
}
