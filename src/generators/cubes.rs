use brush::Brush;
use brush::Rectangle;
use canvas::Canvas;
use color::Color;
use point::Point;
use rand::Rng;

pub fn cubic_disarray(
    canvas: &mut Canvas,
    colors: &mut Vec<Color>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    // Basically, split the "working canvas" into 10 by 10 pieces (there will be 100 squares total)
    let num_splits_x = generations as usize;
    let num_splits_y = (generations) as usize;

    debug!("Number of x splits: {:?}", num_splits_x);
    debug!("Number of y splits: {:?}", num_splits_y);

    let starting_upper_left = Point::new(30, 30);
    let ending_lower_right = Point::new(canvas.size.x - 30, canvas.size.y - 30);

    let difference_x = (starting_upper_left.x - ending_lower_right.x).abs() as usize;
    let difference_y = (starting_upper_left.y - ending_lower_right.y).abs() as usize;

    let modulo_x = (difference_x % num_splits_x) as i32;
    let modulo_y = (difference_y % num_splits_y) as i32;

    // Lets make an array of num_splits_x by num_splits_y rectangles, and make sure they're in the
    // middle.
    let a: Vec<i32> =
        (starting_upper_left.x + (modulo_x / 2) + ((difference_x / num_splits_x / 2) as i32)
            ..ending_lower_right.x + (modulo_x / 2))
            .step_by(difference_x / (num_splits_x))
            .collect();
    let b: Vec<i32> =
        (starting_upper_left.y + (modulo_y / 2) + ((difference_y / num_splits_y / 2) as i32)
            ..ending_lower_right.y + (modulo_y / 2))
            .step_by(difference_y / (num_splits_y))
            .collect();

    debug!("X vals: {:?}", a);
    debug!("Y vals: {:?}", b);

    let mut centers: Vec<Point> = Vec::new();
    let mut rotation: i32 = 0;

    let mut var1 = 0;
    let mut var2 = ((1.0f64 / generations as f64) * 90.0f64) as i32;

    debug!("var1: {:?}, var2: {:?}", var1, var2);

    for y in &b {
        for x in &a {
            let color = *rng.choose(&colors).unwrap();
            let flipped = if (rng.gen_range(0.0, 1.0) > 0.5) {
                1
            } else {
                -1
            };

            let position = Point::new(*x, *y);

            let new_rotation = rotation + rng.gen_range(var1, var2) as i32 * flipped;

            let size = Point::new(
                (difference_x / 2 / num_splits_x) as i32,
                (difference_y / 2 / num_splits_y) as i32,
            );

            debug!(
                "Position: {:?}, Rotation: {:?}, Size: {:?}",
                position, new_rotation, size
            );

            let rectangle = Rectangle {
                stroke_color: color,
                stroke_size: 7,
                rect_size: size,
                rotation: new_rotation,
                ..Default::default()
            };

            rectangle.paint(canvas, Point::new(*x, *y));
        }

        var1 = var2;
        var2 = var2 + ((1.0f64 / generations as f64) * 90.0f64) as i32;
    }
    debug!("All of the centers for cubic disarray:\n{:?}", centers);
}

pub fn cubic_disarray_filled(
    canvas: &mut Canvas,
    colors: &mut Vec<Color>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    // Basically, split the "working canvas" into 10 by 10 pieces (there will be 100 squares total)
    let num_splits_x = generations as usize;
    let num_splits_y = (generations) as usize;

    debug!("Number of x splits: {:?}", num_splits_x);
    debug!("Number of y splits: {:?}", num_splits_y);

    let starting_upper_left = Point::new(30, 30);
    let ending_lower_right = Point::new(canvas.size.x - 30, canvas.size.y - 30);

    let difference_x = (starting_upper_left.x - ending_lower_right.x).abs() as usize;
    let difference_y = (starting_upper_left.y - ending_lower_right.y).abs() as usize;

    let modulo_x = (difference_x % num_splits_x) as i32;
    let modulo_y = (difference_y % num_splits_y) as i32;

    // Lets make an array of num_splits_x by num_splits_y rectangles, and make sure they're in the
    // middle.
    let a: Vec<i32> =
        (starting_upper_left.x + (modulo_x / 2) + ((difference_x / num_splits_x / 2) as i32)
            ..ending_lower_right.x - (modulo_x / 2) - ((difference_x / num_splits_x / 2) as i32))
            .step_by(difference_x / (num_splits_x))
            .collect();
    let b: Vec<i32> =
        (starting_upper_left.y + (modulo_y / 2) + ((difference_y / num_splits_y / 2) as i32)
            ..ending_lower_right.y - (modulo_y / 2) - ((difference_y / num_splits_y / 2) as i32))
            .step_by(difference_y / (num_splits_y))
            .collect();

    debug!("X vals: {:?}", a);
    debug!("Y vals: {:?}", b);

    let mut centers: Vec<Point> = Vec::new();
    let mut rotation: i32 = 0;

    let mut var1 = 0;
    let mut var2 = ((1.0f64 / generations as f64) * 90.0f64) as i32;

    debug!(
        "var1: {:?}, var2: {:?}",
        var1,
        ((1.0f64 / generations as f64) * 90.0f64)
    );

    for y in &b {
        for x in &a {
            let color = *rng.choose(&colors).unwrap();
            let flipped = if (rng.gen_range(0.0, 1.0) > 0.5) {
                1
            } else {
                -1
            };

            let position = Point::new(*x, *y);

            let new_rotation = rotation + rng.gen_range(var1, var2) as i32 * flipped;

            let size = Point::new(
                (difference_x / 2 / num_splits_x) as i32,
                (difference_y / 2 / num_splits_y) as i32,
            );

            debug!(
                "Position: {:?}, Rotation: {:?}, Size: {:?}",
                position, new_rotation, size
            );

            let rectangle = Rectangle {
                stroke_color: color,
                stroke_size: 7,
                rect_size: size,
                rotation: new_rotation,
                fill_color: color,
            };

            rectangle.paint(canvas, Point::new(*x, *y));
        }

        var1 = var2;
        var2 = var2 + ((1.0f64 / generations as f64) * 90.0f64) as i32;
    }
    debug!("All of the centers for cubic disarray:\n{:?}", centers);
}

pub fn sprinkles(
    canvas: &mut Canvas,
    colors: &mut Vec<Color>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    // There will be generations*generations rectangles
    let num_splits_x = (generations as f64 * 1.5) as usize;
    let num_splits_y = generations as usize;

    let starting_upper_left = Point::new(30, 30);
    let ending_lower_right = Point::new(canvas.size.x - 30, canvas.size.y - 30);

    let difference_x = (starting_upper_left.x - ending_lower_right.x).abs() as usize;
    let difference_y = (starting_upper_left.y - ending_lower_right.y).abs() as usize;

    let modulo_x = (difference_x % num_splits_x) as i32;
    let modulo_y = (difference_y % num_splits_y) as i32;

    // Lets make an array of num_splits_x by num_splits_y rectangles, and make sure they're in the
    // middle.
    let a: Vec<i32> =
        (starting_upper_left.x + (modulo_x / 2) + ((difference_x / num_splits_x / 2) as i32)
            ..ending_lower_right.x - (modulo_x / 2) - ((difference_x / num_splits_x / 2) as i32))
            .step_by(difference_x / (num_splits_x))
            .collect();
    let b: Vec<i32> =
        (starting_upper_left.y + (modulo_y / 2) + ((difference_y / num_splits_y / 2) as i32)
            ..ending_lower_right.y - (modulo_y / 2) - ((difference_y / num_splits_y / 2) as i32))
            .step_by(difference_y / (num_splits_y))
            .collect();

    let mut centers: Vec<Point> = Vec::new();
    let mut rotation: f64 = 0.0f64;

    let mut var1 = (0.25f64 * 90.0f64) as i32;
    let mut var2 = (0.35f64 * 90.0f64) as i32;
    debug!("var1: {:?}, var2: {:?}", var1, var2);

    for y in &b {
        for x in &a {
            let color = *rng.choose(&colors).unwrap();
            let flipped = if (rng.gen_range(0.0, 1.0) > 0.5) {
                1
            } else {
                -1
            };

            let new_rotation = rng.gen_range(var1, var2) * flipped;

            debug!("Rotations is set to {:?}", new_rotation);

            let size = Point::new(
                ((difference_x / 2 / num_splits_x) as f64 * 0.25) as i32,
                ((difference_y / 2 / num_splits_y) as f64 * 1.0) as i32,
            );

            let rectangle = Rectangle {
                stroke_color: color,
                stroke_size: 7,
                rect_size: size,
                rotation: new_rotation,
                fill_color: color,
            };

            rectangle.paint(canvas, Point::new(*x, *y));
        }
    }
}
