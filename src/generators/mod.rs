//pub mod pipes;
//pub mod stripes;

use brush::Brush;
use brush::Rectangle;
use canvas::Canvas;
use color::Color;
use painter;
use point::Point;
use rand::Rng;
use svg::node::element;

pub mod circles;
pub mod cubes;
/*pub mod curves;
pub mod pipes;
pub mod stripes;
*/
/// The "main" method that should be called, needs the colors, canvas, and which method it uses to
/// generate the wallpaper.
pub fn generate_image(
    canvas: &mut Canvas,
    colors: &mut Vec<Color>,
    generations: u32,
    generation_method: u8,
) {
    let mut rng = ::rand::thread_rng();

    let base_color = get_base_color(colors);

    // Create our rectanggle object to span the canvas
    let base_rect = Rectangle {
        fill_color: base_color,
        rect_size: canvas.size,
        rotation: 0,
        ..Default::default()
    };

    base_rect.paint(canvas, Point::new((canvas.size.x / 2), (canvas.size.y / 2)));

    // Match the generation type
    match generation_method {
        0 => cubes::cubic_disarray(canvas, colors, &mut rng, generations),
        1 => cubes::cubic_disarray_filled(canvas, colors, &mut rng, generations),
        4 => cubes::sprinkles(canvas, colors, &mut rng, generations),
        5 => circles::circles_in_disarray(canvas, colors, &mut rng, generations),
        6 => circles::circles_in_spiral(canvas, colors, &mut rng, generations),
        /*
        5 => curves::curve(canvas, colors, &mut rng, generations),
        6 => curves::curves(canvas, colors, &mut rng, generations),
        7 => curves::simple_waves(canvas, colors, &mut rng, generations),
        8 => curves::flowers(canvas, colors, &mut rng, generations),
        9 => stripes::perpendicular_stripes(canvas, colors, &mut rng, generations), */
        _ => panic!(),
    }
}

fn get_base_color(colors: &mut Vec<Color>) -> Color {
    let mut rng = ::rand::thread_rng();
    let pixel_index = rng.gen_range(0, colors.len());

    let &ret_color = colors.get(pixel_index).unwrap();

    colors.remove(pixel_index);

    ret_color
}

// Generates a startping point on the north part of the canvas
fn get_north_starting_point(canvas_size: &Point, rng: &mut ::rand::ThreadRng) -> Point {
    let x;
    let y;

    x = rng.gen_range(0, canvas_size.x);
    y = 0;

    Point::new(x, y)
}

// Generates a starting point on the south part of the canvas
fn get_south_starting_point(canvas_size: &Point, rng: &mut ::rand::ThreadRng) -> Point {
    let x;
    let y;

    x = rng.gen_range(0, canvas_size.x);
    y = canvas_size.y;

    Point::new(x, y)
}

// Generates a starting point on the east part of the canvas
fn get_west_starting_point(canvas_size: &Point, rng: &mut ::rand::ThreadRng) -> Point {
    let x;
    let y;

    x = 0;
    y = rng.gen_range(0, canvas_size.y);

    Point::new(x, y)
}

//Generates a starting point on the west part of the canvas
fn get_east_starting_point(canvas_size: &Point, rng: &mut ::rand::ThreadRng) -> Point {
    let x;
    let y;

    x = canvas_size.x;
    y = rng.gen_range(0, canvas_size.y);

    Point::new(x, y)
}
