use brush::Brush;
use brush::Circle;
use canvas::Canvas;
use color::Color;
use point::Point;
use rand::Rng;

pub fn circles_in_disarray(
    canvas: &mut Canvas,
    colors: &mut Vec<Color>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    // Basically, split the "working canvas" into 10 by 10 pieces (there will be 100 squares total)
    let num_splits_x = generations as usize;
    let num_splits_y = (generations) as usize;

    debug!("Number of x splits: {:?}", num_splits_x);
    debug!("Number of y splits: {:?}", num_splits_y);

    let starting_upper_left = Point::new(30, 30);
    let ending_lower_right = Point::new(canvas.size.x - 30, canvas.size.y - 30);

    let difference_x = (starting_upper_left.x - ending_lower_right.x).abs() as usize;
    let difference_y = (starting_upper_left.y - ending_lower_right.y).abs() as usize;

    let modulo_x = (difference_x % num_splits_x) as i32;
    let modulo_y = (difference_y % num_splits_y) as i32;

    // Lets make an array of num_splits_x by num_splits_y rectangles, and make sure they're in the
    // middle.
    let a: Vec<i32> =
        (starting_upper_left.x + (modulo_x / 2) + ((difference_x / num_splits_x / 2) as i32)
            ..ending_lower_right.x + (modulo_x / 2))
            .step_by(difference_x / (num_splits_x))
            .collect();
    let b: Vec<i32> =
        (starting_upper_left.y + (modulo_y / 2) + ((difference_y / num_splits_y / 2) as i32)
            ..ending_lower_right.y + (modulo_y / 2))
            .step_by(difference_y / (num_splits_y))
            .collect();

    debug!("X vals: {:?}", a);
    debug!("Y vals: {:?}", b);

    let mut centers: Vec<Point> = Vec::new();
    let mut rotation: i32 = 0;

    let mut var1 = 0;
    let mut var2 = ((1.0f64 / generations as f64) * 90.0f64) as i32;

    debug!("var1: {:?}, var2: {:?}", var1, var2);

    for y in &b {
        for x in &a {
            let color = *rng.choose(&colors).unwrap();
            let flipped = if (rng.gen_range(0.0, 1.0) > 0.5) {
                1
            } else {
                -1
            };

            let position = Point::new(*x, *y);

            let new_rotation = rotation + rng.gen_range(var1, var2) as i32 * flipped;

            debug!("Position: {:?}, Size: {:?}", position, difference_x);

            let circle = Circle {
                stroke_color: color,
                stroke_size: 2,
                circle_radius: difference_x as i32 / num_splits_x as i32 / 4,

                rotation: new_rotation,
                ..Default::default()
            };

            circle.paint(canvas, Point::new(*x, *y));
        }

        var1 = var2;
        var2 = var2 + ((1.0f64 / generations as f64) * 90.0f64) as i32;
    }
    debug!("All of the centers for cubic disarray:\n{:?}", centers);
}

pub fn circles_in_spiral(
    canvas: &mut Canvas,
    colors: &mut Vec<Color>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    let center = Point::new(canvas.size.x / 2, canvas.size.y / 2);
    let mut placement = center;
    let mut rotation = 0.0f64;
    let biggest_size = if canvas.size.x > canvas.size.y {
        canvas.size.x
    } else {
        canvas.size.y
    };
    let x_change = ((1.0f64 / generations as f64) * canvas.size.x as f64 / 2.0f64) as i32;
    let y_change = ((1.0f64 / generations as f64) * canvas.size.y as f64 / 2.0f64) as i32;

    let change = if x_change < y_change {
        x_change
    } else {
        y_change
    };

    let rotation_step = 1.0f64 / generations as f64;
    let mut actual_rotation = 0.0f64;

    let mut actual_placement = placement;

    for i in (0..generations) {
        debug!("Rotation is set at {:?}", actual_rotation);

        let circle_radius = rng.gen_range(0, (biggest_size as f64 * 0.02f64) as i32);
        let color = *rng.choose(&colors).unwrap();
        let circle = Circle {
            fill_color: color,
            circle_radius: circle_radius,
            rotation: 0,
            ..Default::default()
        };

        circle.paint(canvas, actual_placement);

        //add 1/generations% of x and y to current position

        placement = Point::new(placement.x + change, placement.y + change);
        rotation = (rotation + rotation_step);
        actual_rotation = rotation * std::f64::consts::PI;
        actual_placement = placement.rotate(center, actual_rotation);
    }
}
