use brush;
use canvas::Canvas;
use generators;
use painter;
use point::Point;
use rand::Rng;
use stamp::HollowRectangle;
use std::f64::consts::PI;

pub fn perpendicular_stripes(
    canvas: &mut Canvas,
    colors: &mut Vec<::image::Rgb<u8>>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    for _i in 0..generations {
        info!("On generations: {:?}/{:?}", _i, generations);

        let pixel = *rng.choose(&colors).unwrap();
        let thickness = rng.gen_range(5, 9);
        let block_size = Point::new(thickness, thickness);
        let begin_stroke: Point;
        let end_stroke: Point;

        // Because we will be going up and to the right, we can start on the bottom or left side of
        // the canvas. Both will allow us to move in the direction we want
        if (rng.gen_range(0, 2) == 1) {
            begin_stroke = generators::get_south_starting_point(&canvas.size, rng);
            end_stroke = Point::new(begin_stroke.x, 0);
        } else {
            begin_stroke = generators::get_east_starting_point(&canvas.size, rng);
            end_stroke = Point::new(0, begin_stroke.y);
        }

        debug!(
            "Color of pixel: {:?}, Block size: {:?}, Starting Point: {:?}, Ending Point: {:?}",
            pixel, block_size, begin_stroke, end_stroke
        );

        let mut square_brush = brush::Rectangle {
            color: pixel,
            size: block_size,
            rotation: 1.0,
        };

        painter::paint_line(canvas, &mut square_brush, begin_stroke, end_stroke);
    }
}

pub fn stars(
    canvas: &mut Canvas,
    colors: &mut Vec<::image::Rgb<u8>>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    /*
    info!("Making stars!");

    for _i in 0..generations {
        debug!("On generations: {:?}/{:?}", _i + 1, generations);

        let color = *rng.choose(&colors).unwrap();
        let center_x = rng.gen_range(0, canvas.size.x);
        let center_y = rng.gen_range(0, canvas.size.y);

        let center = Point::new(center_x, center_y);

        debug!("Placing star at {:?}", center);

        let deg_0 = Point::new(center.x, center.y - 11);
        let deg_45 = deg_0.rotate(center, 0.7853982);
        let deg_90 = deg_0.rotate(center, 1.570796);
        let deg_135 = deg_0.rotate(center, 2.356194);
        let deg_180 = deg_0.rotate(center, 3.141593);
        let deg_225 = deg_0.rotate(center, 3.926991);
        let deg_270 = deg_0.rotate(center, 4.712389);
        let deg_315 = deg_0.rotate(center, 5.497787);

        tools::paint_line(canvas, color, deg_0, deg_180);
        tools::paint_line(canvas, color, deg_45, deg_225);
        tools::paint_line(canvas, color, deg_90, deg_270);
        tools::paint_line(canvas, color, deg_135, deg_315);
    } */
}

pub fn test(
    canvas: &mut Canvas,
    colors: &mut Vec<::image::Rgb<u8>>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    /*for _i in 0..generations {
        info!("On generations: {:?}/{:?}", _i, generations);

        let pixel = *rng.choose(&colors).unwrap();
        let block_size = 20; //rng.gen_range(5, 9);
        let slope: Point;
        let mut position: Point;

        // Because we will be going up and to the right, we can start on the bottom or left side of
        // the canvas. Both will allow us to move in the direction we want
        if (rng.gen_range(0, 2) == 1) {
            slope = Point::new(-100, -100);
            position = generators::get_south_starting_point(&canvas.size, rng);
        } else {
            slope = Point::new(-100, -100);
            position = generators::get_east_starting_point(&canvas.size, rng);
        }

        let position = Point::new(250, 250);

        debug!(
            "Color of pixel: {:?}, Block size: {:?}, Starting Point: {:?}, Slope: {:?}",
            pixel, block_size, position, slope
        );

        let mut square_brush = Square {
            color: pixel,
            center: position,
            thickness: block_size,
            rotation: PI / 2.5,
        };

        // Keep painting the block in the direction until out of bounds
        // NOTE: 500 doesn't really matter, because the while loop will
        // cause it to run until completion. the 500 is just so we don't
        // have to increment by 1.
        while (!painter::paint_line(canvas, &mut square_brush, 500, slope)) {}
    }*/
}
/*
pub fn diagonal_stripes(
    canvas: &mut Canvas,
    colors: &mut Vec<::image::Rgb<u8>>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    for _i in 0..generations {
        println!("On generations: {:?}", _i);

        let pixel = *rng.choose(&colors).unwrap();
        let block_size = rng.gen_range(5, 9);
        let direction = directions::gen_diagonal_direction(rng);

        println!("We are going direction: {:?}", direction);

        let (x, y) = generators::starting_points(canvas, rng, direction);

        println!("Our starting position is {:?}", (x, y));

        let mut stripe_block = painter::Block {
            color: pixel,
            x: x,
            y: y,
            thickness: block_size,
        };

        while (!painter::paint_diagonal(canvas, &mut stripe_block, 500, direction)) {}
    }
}

pub fn iterative_diagonal_stripes(
    canvas: &mut Canvas,
    colors: &mut Vec<::image::Rgb<u8>>,
    rng: &mut ::rand::ThreadRng,
) {
    let block_size = rng.gen_range(100, 150) as i32;
    let direction = directions::Directions::NorthWest;
    let (mut x, mut y) = (0, canvas.y_size as i32);

    // In order to get proper stripes, we need to start the block from out of bounds
    // In order to get this, we must set the x to -thickness + 1
    x = ((block_size / 2) * -1) + 2;
    y = y + (block_size / 2) - 2;
    while (x < canvas.x_size as i32) {
        let pixel = *rng.choose(&colors).unwrap();

        let mut block = painter::Block {
            color: pixel,
            x: x,
            y: y,
            thickness: block_size,
        };

        while (!painter::paint_diagonal(canvas, &mut block, 500, direction)) {}

        x += block_size;
    }
}
*/
