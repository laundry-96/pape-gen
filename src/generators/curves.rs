use brush;
use canvas::Canvas;
use painter;
use point::Point;
use rand::Rng;

pub fn curves(
    canvas: &mut Canvas,
    colors: &mut Vec<::image::Rgb<u8>>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    let x = canvas.size.x;
    let y = canvas.size.y;

    for _i in 0..generations {
        let x1 = rng.gen_range(0, x);
        let y1 = rng.gen_range(0, y);
        let x2 = rng.gen_range(0, x);
        let y2 = rng.gen_range(0, y);
        let x3 = rng.gen_range(0, x);
        let y3 = rng.gen_range(0, y);

        let a = Point::new(x1, y1);
        let b = Point::new(x2, y2);
        let c = Point::new(x3, y3);

        let color = *rng.choose(&colors).unwrap();
        let rectangle_brush = brush::Rectangle {
            color: color,
            size: Point::new(5, 5),
            rotation: 0.0f64,
        };

        painter::paint_quadratic_curve(canvas, &rectangle_brush, a, c, b, 500);
    }
}

pub fn curve(
    canvas: &mut Canvas,
    colors: &mut Vec<::image::Rgb<u8>>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    let x = canvas.size.x;
    let y = canvas.size.y;

    let a = Point::new(50, 50);
    let b = Point::new(75, 50);
    let c = Point::new(120, 120);

    let color = *rng.choose(&colors).unwrap();
    let rectangle_brush = brush::Rectangle {
        color: color,
        size: Point::new(5, 5),
        rotation: 0.0f64,
    };

    painter::paint_quadratic_curve(canvas, &rectangle_brush, a, c, b, 500);
}

pub fn simple_waves(
    canvas: &mut Canvas,
    colors: &mut Vec<::image::Rgb<u8>>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    // Basically, split the "working canvas" by generations
    let num_splits_x = 2 * generations as usize;
    let num_splits_y = generations as usize;

    let starting_upper_right = Point::new(30, 30);
    let ending_lower_left = Point::new(canvas.size.x - 30, canvas.size.y - 30);

    let difference_x = (starting_upper_right.x - ending_lower_left.x).abs() as usize;
    let difference_y = (starting_upper_right.y - ending_lower_left.y).abs() as usize;

    let height_max = difference_y / num_splits_y;

    // Lets make an array of num_splits_x by num_splits_y rectangles
    let a: Vec<i64> = (starting_upper_right.x..ending_lower_left.x)
        .skip(difference_x / 2 / (num_splits_x))
        .step_by(difference_x / (num_splits_x))
        .collect();
    let b: Vec<i64> = (starting_upper_right.y..ending_lower_left.y)
        .skip(difference_y / 2 / (num_splits_y))
        .step_by(difference_y / (num_splits_y))
        .collect();

    // For each row
    for y in &b {
        let color = *rng.choose(&colors).unwrap();
        let rectangle_brush = brush::Rectangle {
            color: color,
            size: Point::new(2, 2),
            rotation: 0.0f64,
        };

        let mut height_map: Vec<i64> = Vec::new();

        let mut high = false;

        for _i in 0..a.len() {
            if (high) {
                height_map.push(height_max as i64 / 2);
                high = false;
            } else {
                height_map.push(-1 * height_max as i64 / 2);
                high = true;
            }
        }

        let mut points: Vec<Point> = Vec::new();

        for x in 0..a.len() {
            let begin_x = a[x];

            let height = rng.gen_range(0, height_max as i64);

            let point_a = Point::new(begin_x, *y + height_map[x]);

            points.push(point_a);
        }

        for i in (0..points.len() - 2).step_by(2) {
            let c_x = (points[i].x + points[i + 1].x) as f64 / 2.0;
            let c_y = (points[i].y + points[i + 1].y) as f64 / 2.0;
            let point_c = Point::new(c_x as i64, c_y as i64);

            painter::paint_quadratic_curve(
                canvas,
                &rectangle_brush,
                points[i],
                points[i + 2],
                points[i + 1],
                1000,
            );
            //painter::paint_quadratic_curve(canvas, &rectangle_brush, points[i], points[i+2], points[i+1], 100);
        }
    }
}

pub fn flowers(
    canvas: &mut Canvas,
    colors: &mut Vec<::image::Rgb<u8>>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    // Basically, split the "working canvas" by generations
    let num_splits_x = 2 * generations as usize;
    let num_splits_y = generations as usize;

    let starting_upper_right = Point::new(30, 30);
    let ending_lower_left = Point::new(canvas.size.x - 30, canvas.size.y - 30);

    let difference_x = (starting_upper_right.x - ending_lower_left.x).abs() as usize;
    let difference_y = (starting_upper_right.y - ending_lower_left.y).abs() as usize;

    let height_max = (difference_y / num_splits_y) as i64;
    let width_max = (difference_x / num_splits_x) as i64;

    // Lets make an array of num_splits_x by num_splits_y rectangles
    let a: Vec<i64> = (starting_upper_right.x..ending_lower_left.x)
        .skip(difference_x / 2 / (num_splits_x))
        .step_by(difference_x / (num_splits_x))
        .collect();
    let b: Vec<i64> = (starting_upper_right.y..ending_lower_left.y)
        .skip(difference_y / 2 / (num_splits_y))
        .step_by(difference_y / (num_splits_y))
        .collect();

    // For each row
    for y in &b {
        let color = *rng.choose(&colors).unwrap();
        let rectangle_brush = brush::Rectangle {
            color: color,
            size: Point::new(2, 2),
            rotation: 0.0f64,
        };

        let mut height_map: Vec<i64> = Vec::new();

        let mut high = false;

        for _i in 0..a.len() {
            if (high) {
                height_map.push(height_max as i64 / 2);
                high = false;
            } else {
                height_map.push(-1 * height_max as i64 / 2);
                high = true;
            }
        }

        let mut points: Vec<Point> = Vec::new();

        // For each column
        for x in &a {
            let mut rotation = 0.0f64;
            let center = Point::new(x + height_max / 2, y + width_max / 2);

            for pedal in 0..20 {
                let mut top =
                    Point::new(x - height_max / 8, y + width_max / 8).rotate(center, rotation);

                let mut c =
                    Point::new(x - height_max / 16, y + width_max / 16).rotate(center, rotation);

                painter::paint_quadratic_curve(canvas, &rectangle_brush, center, top, c, 100);

                rotation += std::f64::consts::PI / 10.0f64;
            }
        }
        return;
    }
}
