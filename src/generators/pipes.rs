use canvas::Canvas;
use directions;
use generators;
use painter;
use rand::Rng;
/*
pub fn pipes(
    canvas: &mut Canvas,
    colors: &mut Vec<::image::Rgb<u8>>,
    rng: &mut ::rand::ThreadRng,
    generations: u32,
) {
    let pipe_min = 50;
    let pipe_max = 100;

    // For each pipe we make
    for _i in 0..generations {
        // Choose a color, block size, direction, and starting point
        let pixel = *rng.choose(&colors).unwrap();
        let block_size = rng.gen_range(5, 9);
        let mut direction = directions::gen_cardinal_direction(rng);

        let (x, y) = generators::starting_points(canvas, rng, direction);

        let mut stripe_block = painter::Block {
            color: pixel,
            x: x,
            y: y,
            thickness: block_size,
        };

        let mut max_steps = rng.gen_range(pipe_min, pipe_max);

        // While we are still within the image
        while (!painter::paint_line(canvas, &mut stripe_block, max_steps, direction)) {
            //Create a new direction, and more pipes
            max_steps = rng.gen_range(pipe_min, pipe_max);
            direction = directions::new_cardinal_direction(direction, rng);
        }
    }
}
*/
