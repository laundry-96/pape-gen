use log::info;
use point::Point;
use resvg::prelude::*;
use svg::node::Node;
use svg::Document;

/// This holds the image object, only because it stores the x and y with it, instead of having
/// to extract it every time we want to see the size
/// The image must have a lifetime applied to it, because it gets reused in main to save the image.
pub struct Canvas {
    pub size: Point,
    pub svg: Document,
}

impl Canvas {
    /// Creates a new Canvas item, mostly just boiler plate, and also so we don't have to
    /// extrapolate with ImageBuffer in the calling class.
    pub fn new(x: i32, y: i32) -> Canvas {
        if (x <= 0 || y <= 0) {
            panic!("X or Y is negative for creating a Canvas. Please make it Positive. X: {:?}, Y: {:?}", x, y);
        }

        info!("Creating a Canvas of size {:?}, {:?}", x, y);

        Canvas {
            size: Point::new(x, y),
            svg: Document::new().set("width", x).set("height", y),
        }
    }

    pub fn add(&mut self, elem: impl Node) {
        // SVG Document won't let us just append, because it returns itself...
        let mut new_svg = self.svg.clone();
        new_svg = new_svg.add(elem);
        self.svg = new_svg;
    }

    /// Saves the file
    pub fn save(&self, file_name: &str) {
        let svg_file = format!("{}.svg", file_name);
        let png_file = format!("{}.png", file_name);

        info!("Saving image to file {:?}", file_name);

        svg::save(svg_file, &self.svg).unwrap();
    }
}

#[test]
fn canvas_test_1() {
    let x: i32 = 100;
    let y: i32 = 100;

    let _canvas = Canvas::new(x, y);
}

#[test]
#[should_panic]
fn canvas_test_2() {
    let x: i32 = 0;
    let y: i32 = 1;

    let _canvas = Canvas::new(x, y);
}

#[test]
#[should_panic]
fn canvas_test_3() {
    let x: i32 = 1;
    let y: i32 = 0;

    let _canvas = Canvas::new(x, y);
}

#[test]
#[should_panic]
fn canvas_test_4() {
    let x: i32 = -100;
    let y: i32 = -100;

    let _canvas = Canvas::new(x, y);
}
