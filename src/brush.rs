use canvas::Canvas;
use color::Color;
use log::{debug, info};
use point::Point;
use svg::node::element;

pub trait Brush {
    fn paint(&self, canvas: &mut Canvas, center: Point);
}

pub struct Rectangle {
    /// Color to fill the rectangle with
    pub fill_color: Color,

    /// Color to paint the edges of the rectangle with
    pub stroke_color: Color,

    /// The size is how big it is.
    pub rect_size: Point,

    pub stroke_size: i32,

    /// Rotation of the brush, in degrees
    pub rotation: i32,
}

impl Default for Rectangle {
    fn default() -> Self {
        Rectangle {
            fill_color: Default::default(),
            stroke_color: Default::default(),
            rect_size: Point::new(50, 50),
            stroke_size: 0,
            rotation: 0,
        }
    }
}

impl Brush for Rectangle {
    /// Paint  on the canvas
    /// Returns true if we were able to paint on the canvas, false otherwise
    fn paint(&self, canvas: &mut Canvas, center: Point) {
        info!("Painting a Rectangle: ");

        // Note: The origin, (0, 0),  is upper left, so when trying to go up, we must subtract, and
        // to go left, we must subtract, right and down are both addition
        let mut upper_left = Point::new(
            center.x - (self.rect_size.x / 2),
            center.y - (self.rect_size.y / 2),
        );

        let transform = format!("rotate({}, {}, {})", self.rotation, center.x, center.y);

        let svgRect = element::Rectangle::new()
            .set("x", upper_left.x.to_string())
            .set("y", upper_left.y.to_string())
            .set("width", self.rect_size.x.to_string())
            .set("height", self.rect_size.y.to_string())
            .set("fill", format!("rgba({})", self.fill_color.to_string()))
            .set("stroke", format!("rgba({})", self.stroke_color.to_string()))
            .set("transform", transform);

        debug!("Rect: {:?}", svgRect);

        canvas.add(svgRect);
    }
}

pub struct Circle {
    /// Color to fill the circle with
    pub fill_color: Color,

    /// Color to paint the circumference of the circle,
    pub stroke_color: Color,

    ///The size of how big the circle will be
    pub circle_radius: i32,

    ///Size of the stroke
    pub stroke_size: i32,

    /// Rotation of the brush in degrees
    pub rotation: i32,
}

impl Default for Circle {
    fn default() -> Self {
        Circle {
            fill_color: Default::default(),
            stroke_color: Default::default(),
            circle_radius: 50,
            stroke_size: 0,
            rotation: 0,
        }
    }
}

impl Brush for Circle {
    fn paint(&self, canvas: &mut Canvas, center: Point) {
        info!("Painting a circle");

        let transform = format!("rotate({}, {}, {})", self.rotation, center.x, center.y);

        let svgCirc = element::Circle::new()
            .set("cx", center.x)
            .set("cy", center.y)
            .set("r", self.circle_radius)
            .set("fill", format!("rgba({})", self.fill_color.to_string()))
            .set("stroke", format!("rgba({})", self.stroke_color.to_string()))
            .set("stroke-width", self.stroke_size.to_string())
            .set("transform", transform);

        debug!("Circle: {:?}", svgCirc);

        canvas.add(svgCirc);
    }
}
