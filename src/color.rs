use std::fmt;

#[derive(Copy, Clone, Debug)]
pub struct Color {
    /// Red of the pixel
    pub red: u8,

    /// Green of the pixel
    pub green: u8,

    /// Blue of the pixel
    pub blue: u8,

    /// Alpha of the pixel (this turns into a float)
    pub alpha: u8,
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let a = self.alpha as f64 / 255.0f64;
        write!(f, "{}, {}, {}, {}", self.red, self.green, self.blue, a)
    }
}

impl Default for Color {
    fn default() -> Self {
        Color {
            red: 0,
            green: 0,
            blue: 0,
            alpha: 0,
        }
    }
}
