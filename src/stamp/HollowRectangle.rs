use brush::*;
use canvas::Canvas;
use image::Rgb;
use log::{debug, info};
use painter;
use point::Point;
use stamp;

pub struct HollowRectangle {
    /// The center of the stamp
    pub center: Point,

    /// The size is how big it is.
    /// We are using a point, so we can determine the the x from the center and y from the center
    /// independently.
    pub size: Point,

    /// Rotation of the stamp, in radians
    pub rotation: f64,
}

impl stamp::Stamp for HollowRectangle {
    /// Stamp  on the canvas
    /// Returns true if we were able to stamp on the canvas, false otherwise
    fn impress(&self, canvas: &mut Canvas, brush: &impl Brush) -> bool {
        info!("Stamping a HollowRectangle");

        // Note: The origin, (0, 0),  is upper left, so when trying to go up, we must subtract, and
        // to go left, we must subtract, right and down are both addition
        let mut upper_left = Point::new(self.center.x - self.size.x, self.center.y - self.size.y);

        let mut upper_right = Point::new(self.center.x - self.size.x, self.center.y + self.size.y);
        let mut bottom_left = Point::new(self.center.x + self.size.x, self.center.y - self.size.y);
        let mut bottom_right = Point::new(self.center.x + self.size.x, self.center.y + self.size.y);

        upper_left = upper_left.rotate(self.center, self.rotation);
        upper_right = upper_right.rotate(self.center, self.rotation);
        bottom_right = bottom_right.rotate(self.center, self.rotation);
        bottom_left = bottom_left.rotate(self.center, self.rotation);

        painter::paint_line(canvas, brush, upper_left, upper_right);
        painter::paint_line(canvas, brush, upper_right, bottom_right);
        painter::paint_line(canvas, brush, bottom_right, bottom_left);
        painter::paint_line(canvas, brush, bottom_left, upper_left);

        return false;
    }
}
/*
impl HollowRectangle {
    fn rotate(&mut self, radians: f64) {
        self.rotation += radians;
    }

    fn update_pos(&mut self, add_pos: Point) {
        self.center += add_pos;
    }

    fn move_to_pos(&mut self, new_pos: Point) {
        self.center = new_pos;
    }
}*/
