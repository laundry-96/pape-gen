use brush::*;
use canvas::Canvas;
use image::Rgb;
use log::{debug, info};
use painter;
use point::Point;
use stamp;

pub struct FilledRectangle {
    /// The center of the stamp
    pub center: Point,

    /// The size is how big it is.
    /// We are using a point, so we can determine the the x from the center and y from the center
    /// independently.
    pub size: Point,

    /// Rotation of the stamp, in radians
    pub rotation: f64,
}

impl stamp::Stamp for FilledRectangle {
    /// Stamp  on the canvas
    /// Returns true if we were able to stamp on the canvas, false otherwise
    fn impress(&self, canvas: &mut Canvas, brush: &impl Brush) -> bool {
        info!("Stamping a HollowRectangle");

        // Note: The origin, (0, 0),  is upper left, so when trying to go up, we must subtract, and
        // to go left, we must subtract, right and down are both addition
        let mut top_left = Point::new(self.center.x - self.size.x, self.center.y - self.size.y);

        let mut top_right = Point::new(self.center.x - self.size.x, self.center.y + self.size.y);
        let mut bottom_left = Point::new(self.center.x + self.size.x, self.center.y - self.size.y);
        let mut bottom_right = Point::new(self.center.x + self.size.x, self.center.y + self.size.y);

        top_left = top_left.rotate(self.center, self.rotation);
        top_right = top_right.rotate(self.center, self.rotation);
        bottom_right = bottom_right.rotate(self.center, self.rotation);
        bottom_left = bottom_left.rotate(self.center, self.rotation);

        let left_points = painter::get_line_points(top_left, bottom_left);
        let right_points = painter::get_line_points(top_right, bottom_right);

        if (left_points.len() != right_points.len()) {
            debug!("Left: {:?}", left_points);
            debug!("Right: {:?}", right_points);
            panic!("This ain't right");
        }

        for i in 0..left_points.len() {
            painter::paint_line(canvas, brush, left_points[i], right_points[i]);
        }

        return false;
    }
}
