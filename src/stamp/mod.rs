use brush::*;
use canvas::Canvas;
use color::Color;
use log::{debug, info};
use painter;
use point::Point;

pub mod FilledRectangle;
pub mod HollowRectangle;

/// Stamp.rs
///
/// Stamp defined by Merriam-Webster
/// to cut out, bend, form with a stamp or die/to provide with a distinctive character
///
/// In the sense for Pape-gen, a stamp is a pre-defined shape that is to be placed onto the canvas. This
/// differs from a brush, as a brush is used to perform various strokes to the canvas. Furthermore, a stamp
/// uses a brush to draw onto the canvas, which means that a stamp cannot be a brush.
///
/// Think of a stamp as general directions to draw something.

pub trait Stamp {
    fn impress(&self, canvas: &mut Canvas, brush: &impl Brush) -> bool;
}
