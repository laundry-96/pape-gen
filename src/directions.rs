use rand::Rng;

#[derive(Eq, PartialEq, Clone, Copy, Debug)]

pub enum Directions {
    NorthWest,
    North,
    NorthEast,
    East,
    SouthEast,
    South,
    SouthWest,
    West,
}

/// Just get a random direction, default returns North
///
pub fn gen_cardinal_direction(rng: &mut ::rand::ThreadRng) -> Directions {
    return match rng.gen_range(0, 4) {
        0 => Directions::North,
        1 => Directions::East,
        2 => Directions::South,
        3 => Directions::West,
        _ => Directions::North,
    };
}

pub fn gen_diagonal_direction(rng: &mut ::rand::ThreadRng) -> Directions {
    return match rng.gen_range(0, 4) {
        0 => Directions::NorthEast,
        1 => Directions::SouthEast,
        2 => Directions::SouthWest,
        3 => Directions::NorthWest,
        _ => Directions::NorthEast,
    };
}

pub fn new_cardinal_direction(cur_dir: Directions, rng: &mut ::rand::ThreadRng) -> Directions {
    let mut curr_direction = cur_dir;
    let past_direction = curr_direction;
    let new_dir = rng.gen_range(0, 2);

    if (past_direction == Directions::North || past_direction == Directions::South) {
        if (new_dir == 0) {
            curr_direction = Directions::East;
        } else {
            curr_direction = Directions::West;
        }
    } else {
        if (new_dir == 0) {
            curr_direction = Directions::North;
        } else {
            curr_direction = Directions::South;
        }
    }

    curr_direction
}
