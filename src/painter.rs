use log::debug;
use svg::node::element;

use canvas::Canvas;

/// Paints all of canvas with a rectangle
pub fn fill_canvas(canvas: &mut Canvas, canvas_base: element::Rectangle) {
    debug!("Filling the canvas with {:?}", canvas_base);

    canvas.add(canvas_base);
}
/*
/// This function uses Breseham's line drawing function :)
/// It was taken from Wikipedia
/// (https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm#Algorithm)

pub fn paint_line(canvas: &mut Canvas, brush: &impl Brush, point_a: Point, point_b: Point) {
    debug!("Painting a line from {:?} to {:?}", point_a, point_b);

    if (point_a == point_b) {
        debug!("Attempting to draw a line to the same points... Skipping...");
        return;
    }

    if ((point_b.y - point_a.y).abs() < (point_b.x - point_a.x).abs()) {
        if (point_a.x > point_b.x) {
            paint_line_low(canvas, brush, point_b, point_a);
        } else {
            paint_line_low(canvas, brush, point_a, point_b);
        }
    } else {
        if (point_a.y > point_b.y) {
            paint_line_high(canvas, brush, point_b, point_a)
        } else {
            paint_line_high(canvas, brush, point_a, point_b);
        }
    }
}

pub fn paint_quadratic_curve(
    canvas: &mut Canvas,
    brush: &impl Brush,
    point_begin: Point,
    point_end: Point,
    point_control: Point,
    steps: u32,
) {
    let mut point_next = Point::new(0, 0);

    // steps + 1 so we make sure we get to the end (just like drawing lines
    for i in 0..steps + 1 {
        let t = (1.0f64 / steps as f64) * (i as f64);

        debug!("On step {:?}/{:?}. T is {:?}", i, steps, t);

        compute_bezier_quadratic_curve(point_begin, point_end, point_control, &mut point_next, t);
        debug!("point_next is {:?}", point_next);
        brush.paint(canvas, point_next);
    }
}

pub fn compute_bezier_quadratic_curve(
    point_begin: Point,
    point_end: Point,
    point_control: Point,
    point_next: &mut Point,
    t: f64,
) {
    point_next.x = (((1.0 - t).powi(2)) * point_begin.x as f64
        + ((1.0 - t) * 2.0 * t) * point_control.x as f64
        + (t * t) * point_end.x as f64) as i64;

    point_next.y = (((1.0 - t).powi(2)) * point_begin.y as f64
        + ((1.0 - t) * 2.0 * t) * point_control.y as f64
        + (t * t) * point_end.y as f64) as i64;

    debug!("in compute: computed: {:?}", point_next);
}

fn compute_bezier_cubic_curve(
    canvas: &mut Canvas,
    brush: &impl Brush,
    point_begin: Point,
    point_end: Point,
    point_control_a: Point,
    point_control_b: Point,
    point_next: &mut Point,
    t: f64,
) {
    point_next.x = ((1.0 - t).powi(3) as i64) * point_begin.x
        + ((1.0 - t).powi(2) * 3.0f64 * t) as i64 * point_control_a.x
        + ((1.0 - t) * 3.0 * t * t) as i64 * point_control_b.x
        + ((t * t) * t) as i64 * point_end.x;

    point_next.y = ((1.0 - t).powi(3) as i64) * point_begin.y
        + ((1.0 - t).powi(2) * 3.0f64 * t) as i64 * point_control_a.y
        + ((1.0 - t) * 3.0 * t * t) as i64 * point_control_b.y
        + ((t * t) * t) as i64 * point_end.y;
}

fn paint_line_high(canvas: &mut Canvas, brush: &impl Brush, point_a: Point, point_b: Point) {
    let mut dx = point_b.x - point_a.x;
    let mut dy = point_b.y - point_a.y;
    let mut xi = 1;
    if (dx < 0) {
        xi = -1;
        dx = -dx;
    }

    let mut D = 2 * dx - dy;
    let mut curr_x = point_a.x;

    for curr_y in point_a.y..(point_b.y + 1) {
        let curr_point = Point::new(curr_x, curr_y);

        // Paint no matter what, we may start off the canvas, but eventually go on.
        brush.paint(canvas, curr_point);

        if (D > 0) {
            curr_x = curr_x + xi;
            D = D - 2 * dy;
        }

        D = D + 2 * dx;
    }
}

fn paint_line_low(canvas: &mut Canvas, brush: &impl Brush, point_a: Point, point_b: Point) {
    let mut dx = point_b.x - point_a.x;
    let mut dy = point_b.y - point_a.y;
    let mut yi = 1;

    if (dy < 0) {
        yi = -1;
        dy = -dy;
    }

    let mut D = 2 * dy - dx;
    let mut curr_y = point_a.y;

    for curr_x in point_a.x..(point_b.x + 1) {
        let curr_point = Point::new(curr_x, curr_y);

        // Paint no matter what, we may start off the canvas, but eventually go on.
        brush.paint(canvas, curr_point);

        if (D > 0) {
            curr_y = curr_y + yi;
            D = D - 2 * dx;
        }
        D = D + 2 * dy;
    }
}

pub fn get_line_points(point_a: Point, point_b: Point) -> Vec<Point> {
    if (point_a == point_b) {
        debug!("Attempting to draw a line to the same points... Skipping...");
        return Vec::new();
    }

    if ((point_b.y - point_a.y).abs() < (point_b.x - point_a.x).abs()) {
        if (point_a.x > point_b.x) {
            get_line_low_points(point_b, point_a)
        } else {
            get_line_low_points(point_a, point_b)
        }
    } else {
        if (point_a.y > point_b.y) {
            get_line_high_points(point_b, point_a)
        } else {
            get_line_high_points(point_a, point_b)
        }
    }
}

fn get_line_high_points(point_a: Point, point_b: Point) -> Vec<Point> {
    let mut dx = point_b.x - point_a.x;
    let mut dy = point_b.y - point_a.y;
    let mut xi = 1;

    let mut points = Vec::new();

    if (dx < 0) {
        xi = -1;
        dx = -dx;
    }

    let mut D = 2 * dx - dy;
    let mut curr_x = point_a.x;

    for curr_y in point_a.y..(point_b.y + 1) {
        let curr_point = Point::new(curr_x, curr_y);

        points.push(curr_point);

        if (D > 0) {
            curr_x = curr_x + xi;
            D = D - 2 * dy;
        }

        D = D + 2 * dx;
    }

    return points;
}

fn get_line_low_points(point_a: Point, point_b: Point) -> Vec<Point> {
    let mut dx = point_b.x - point_a.x;
    let mut dy = point_b.y - point_a.y;
    let mut yi = 1;

    let mut points = Vec::new();

    if (dy < 0) {
        yi = -1;
        dy = -dy;
    }

    let mut D = 2 * dy - dx;
    let mut curr_y = point_a.y;

    for curr_x in point_a.x..(point_b.x + 1) {
        let curr_point = Point::new(curr_x, curr_y);

        points.push(curr_point);

        if (D > 0) {
            curr_y = curr_y + yi;
            D = D - 2 * dx;
        }
        D = D + 2 * dy;
    }

    return points;
}
*/
