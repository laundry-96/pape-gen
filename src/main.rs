#![allow(unused_parens)]

extern crate num_traits;
extern crate pretty_env_logger;
extern crate rand;
extern crate resvg;
extern crate structopt;
extern crate svg;
#[macro_use]
extern crate log;

use num_traits::Num;
use std::fs::File;
use std::io::prelude::*;
use structopt::StructOpt;

pub mod brush;
pub mod canvas;
pub mod color;
pub mod directions;
pub mod generators;
pub mod painter;
pub mod point;

use canvas::Canvas;

#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
struct Opt {
    // File with colors
    #[structopt(short = "p", long = "palette", default_value = "palettes/colors.txt")]
    palette: String,
    #[structopt(short = "x", long = "x", default_value = "500")]
    x: i32,
    #[structopt(short = "y", long = "y", default_value = "500")]
    y: i32,
    #[structopt(short = "i", long = "iterations", default_value = "500")]
    iterations: u32,
    #[structopt(short = "g", long = "generator", default_value = "0")]
    generator: u8,
    #[structopt(short = "o", long = "out", default_value = "out")]
    output: String,
}

fn main() {
    pretty_env_logger::init_timed();

    let opt = Opt::from_args();
    info!("Using values: Palatte: {:?}, x: {:?}, y: {:?}, iterations: {:?}, generator: {:?}, file_out: {:?}", &opt.palette, opt.x, opt.y, opt.iterations, opt.generator, opt.output);

    let mut colors = load_colors(&opt.palette);
    let x_size = opt.x;
    let y_size = opt.y;
    let iterations = opt.iterations;
    let generator = opt.generator;
    let file_name = opt.output;

    let mut canvas = Canvas::new(x_size, y_size);

    generators::generate_image(&mut canvas, &mut colors, iterations, generator);

    canvas.save(file_name.as_str());
}

fn load_colors(file: &str) -> Vec<color::Color> {
    info!("We are loading the pallete file {:?}", file);

    let mut colors_file = File::open(file).expect("File not found");
    let mut contents = String::new();
    colors_file
        .read_to_string(&mut contents)
        .expect("Data not readable");
    let colors_str: Vec<&str> = contents.trim().split("\n").collect();
    let mut colors: Vec<color::Color> = Vec::new();
    for color in &colors_str {
        let red = <u8 as Num>::from_str_radix(color.get(0..2).unwrap(), 16).unwrap();
        let green = <u8 as Num>::from_str_radix(color.get(2..4).unwrap(), 16).unwrap();
        let blue = <u8 as Num>::from_str_radix(color.get(4..6).unwrap(), 16).unwrap();
        let alpha = <u8 as Num>::from_str_radix(color.get(6..8).unwrap_or("ff"), 16).unwrap();
        colors.push(color::Color {
            red: red,
            green: green,
            blue: blue,
            alpha: alpha,
        });
    }

    debug!("The colors in the pallete are: {:?}", colors);

    return colors;
}
