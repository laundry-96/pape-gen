# pape-gen
`pape-gen` is a simple generative art program. It handles all the 'painting' with SVG, to minimize compute time of writing to a bitmap. Writing to a bitmap was how it was done before, but when creating bigger files, and adding more complex 'art', it took longer. Also, SVG has many of the shapes built in for us, so we don't need to reinvent the wheel.

## Options

### Running
In order to run the program, the following flags must be specified (any order)

Optional flags:

* `--file` | `-f`
  * Specify the file to write to
  * Default is `./out.png`
* `-x`
  * The X boundry of the image
  * The default is 500px
* `-y`
  * The Y boundry of the image
  * The default is 500px
* `-i`
  * How many iterations you'd like. This usually translates to how many "objects" drawn or something (check the source!).
  * The default is 500
* `-p`
  * The palette file to use (there are a ton in the palettes/ directory).
  * The default is `palettes/colors.txt`
  * This file is relative to $PWD!
* `-g`
  * Which generator you want to use (check src/generators/mod.rs for a full list).
  * The default is 0

Examples:
```bash
cargo run -- -g 7 -i 10 -p palettes/blues.txt -x 2880 -y 1800
cargo run -- -g 3 -i 20 -p palettes/black_white.txt```
```

### Installing
I am not really sure how to do this, as I've just started development in rust, there may be an install later so you can generate a new wallpaper every time you log in by putting the command in a login script and changing the wallpaper that way, but that will be the user's project.

### Contributing
I am currently looking for more algorithms to generate wallpapers! Currently implemented pipes and stripes!

## Authors
**Austin DeLauney** - _Initial Work_ - [laundry-96](https://gitlab.com/laundry-96)

## Acknowledgments
  - rust IRC for helping me out with random questions
